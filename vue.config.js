const path = require('path');
// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const engine = require('./server/engine.js');
const routes = require('./server/routes.js');

const isProd = process.env.NODE_ENV === 'production';
const isDev = process.env.NODE_ENV === 'development';

const addStyleResource = (rule) => {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [
        path.resolve(__dirname, './src/assets/stylus/variables.styl'),
        path.resolve(__dirname, './src/assets/stylus/responsive.styl'),
      ],
    });
};

// vue.config.js
module.exports = {
  publicPath: isProd ? '/embeds/test/build' : '/embeds/test/',
  productionSourceMap: false,
  chainWebpack: (config) => {
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];
    types.forEach((type) => addStyleResource(config.module.rule('stylus').oneOf(type)));
    config.optimization.delete('splitChunks');
  },
  pages: {
    main: 'src/main.js',
    admin: 'src/admin.js',
    util: 'src/util.js',
  },
  configureWebpack: {
    devtool: 'source-map',
    output: {
      filename: (pathData) => (isDev || pathData.chunk.name === 'util' ? '[name].js' : '[name].[hash].js'),
    },
    plugins: [
      new HtmlWebPackPlugin({
        inject: false,
        filename: path.resolve(process.cwd(), './views/partials/headTags.hbs'),
        templateContent: ({ htmlWebpackPlugin }) => `${htmlWebpackPlugin.tags.headTags}`,
      }),
      new HtmlWebPackPlugin({
        inject: false,
        chunks: ['main', 'util'],
        filename: path.resolve(process.cwd(), './views/partials/bodyTags.hbs'),
        templateContent: ({ htmlWebpackPlugin }) => `${htmlWebpackPlugin.tags.bodyTags}`,
      }),
      new HtmlWebPackPlugin({
        inject: false,
        chunks: ['admin'],
        filename: path.resolve(process.cwd(), './views/partials/bodyTagsAdmin.hbs'),
        templateContent: ({ htmlWebpackPlugin }) => `${htmlWebpackPlugin.tags.bodyTags}`,
      }),
      new webpack.HotModuleReplacementPlugin(),
    ],
  },
  css: {
    sourceMap: false,
    extract: false,
  },
  devServer: {
    before: (app) => {
      engine(app);
      app.use('/embeds/test', routes);
    },
    port: 8090,
    publicPath: '/embeds/test/build/',
    hot: true,
  },
};
