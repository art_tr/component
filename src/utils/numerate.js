export default function numerate(srt, count = 0) {
  let result = '';
  const variants = srt.split(':::');
  const first = variants[0] || '';
  const second = variants[1] || '';
  const third = variants[2] || second;
  const empty = variants[3] || third;

  const abscount = Math.abs(count);

  if (abscount.toString().indexOf('.') > -1) {
    result = second;
  } else if (!abscount) {
    result = empty;
  } else if (abscount % 10 === 1 && abscount % 100 !== 11) {
    result = first;
  } else if (
    abscount % 10 >= 2
    && abscount % 10 <= 4
    && (abscount % 100 < 10 || abscount % 100 >= 20)
  ) {
    result = second;
  } else {
    result = third;
  }

  return result;
}
