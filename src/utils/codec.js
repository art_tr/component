const AES = require('crypto-js/aes');
const encUtf8 = require('crypto-js/enc-utf8');
const encBase64 = require('crypto-js/enc-base64');
const encHex = require('crypto-js/enc-hex');

const SECRET = 'visit careers.tribuna.digital, we\'re hiring!';

const decode = (cipherText) => {
  try {
    const reb64 = encHex.parse(cipherText);
    const bytes = reb64.toString(encBase64);
    const decrypt = AES.decrypt(bytes, SECRET);
    const plain = decrypt.toString(encUtf8);
    return JSON.parse(plain);
  } catch (error) {
    return null;
  }
};

const encode = (data) => {
  const b64 = AES.encrypt(JSON.stringify(data), SECRET).toString();
  const e64 = encBase64.parse(b64);
  const eHex = e64.toString(encHex);
  return eHex;
};

module.exports = {
  decode,
  encode,
};
