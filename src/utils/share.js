export default {
  getLink(type, customOptions = {}) {
    const options = {
      ...this.getDefault(),
      ...customOptions,
    };
    let link = '';
    const text = `${options.title} ${options.description}`;
    switch (type) {
      case 'fb':
        link = `https://www.facebook.com/sharer/sharer.php?u=${options.url}`;
        break;
      case 'vk':
        link = `https://vk.com/share.php?url=${options.url}&title=${options.title}&description=${options.description}&image=${options.image}`;
        break;
      case 'tw':
        link = `https://twitter.com/intent/tweet?url=${options.url}&text=${options.title}`;
        break;
      case 'tg':
        link = `tg://share?url=${options.url}&text=${encodeURIComponent(text)}`;
        break;
      default:
        break;
    }

    return link;
  },
  open(type, customOptions = {}) {
    const windowParams = 'width=630&height=340&personalbar=0&toolbar=no&scrollbars=no&resizable=yes&location=no';
    const left = Math.floor((window.screen.width / 2) - (630 / 2));
    const right = Math.floor(window.screen.height / 5);
    const link = this.getLink(type, customOptions);

    if (link) {
      window.open(link, 'share_dialog', `${windowParams.replace(/&/g, ',')},left=${left},top=${right}`);
    }
  },

  getDefault() {
    const metaDescription = document.querySelector('meta[name="description"]');
    return {
      url: window.location.href,
      title: document.title,
      description: metaDescription ? metaDescription.content : '',
    };
  },
};
