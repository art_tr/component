export default {
  send({
    name = '', label = '', id, value,
  }) {
    if (!window.specialDataLayer) window.specialDataLayer = [];
    const data = {
      event: `special-basic-test-${id}`,
      eventCategory: `special-basic-test-${id}`,
      eventLabel: label,
      eventName: name,
    };
    if (value) data.eventValue = value;
    window.specialDataLayer.push(data);
  },
};
