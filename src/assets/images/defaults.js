export default class DefaultImages {
  static startBackground = [
    {
      label: 'Пользовательский фон',
      url: '',
    },
    {
      label: 'Футбольное покрытие',
      url: 'https://s5o.ru/storage/dumpster/3/b0/352b423f16b5b1288641bf900e55d.jpg',
    },
    {
      label: 'Стадион',
      url: 'https://s5o.ru/storage/dumpster/7/79/03b22478db2ee92ea493d0464a2ff.jpg',
    },
    {
      label: 'Футбольное покрытие 2',
      url: 'https://s5o.ru/storage/dumpster/f/0d/1a58d8abd37040291ad52146fe917.jpg',
    },
    {
      label: 'Удар по мячу',
      url: 'https://s5o.ru/storage/dumpster/3/81/73d252bff0cae5df87610dce9b00d.jpg',
    },
    {
      label: 'Болельщики на трибунах',
      url: 'https://s5o.ru/storage/dumpster/8/14/2fcaac0998b35ee8b41a9aca2813a.jpg',
    },
    {
      label: 'Трибуны',
      url: 'https://s5o.ru/storage/dumpster/1/38/6c27de9187ab716452ee0ce5995bf.jpg',
    },
  ];

  static shareBackground = [
    'https://s5o.ru/storage/dumpster/9/86/4961856978d699a788a7784213d8a.jpg',
    'https://s5o.ru/storage/dumpster/e/d3/afbac5ab01378d7803a2858b82a65.jpg',
    'https://s5o.ru/storage/dumpster/4/8c/5c103ad924b1ad100e9c4024404eb.jpg',
    'https://s5o.ru/storage/dumpster/f/14/61c36bc01807d1332e4b92f3ccaf0.jpg',
    'https://s5o.ru/storage/dumpster/8/fa/65e61fc067348a7efd90f28ba71c2.jpg',
    'https://s5o.ru/storage/dumpster/6/c7/ec52adad5d07b08ea2679ca55a6ca.jpg',
  ];

  static getShareBackground() {
    return this.shareBackground[Math.floor(Math.random() * this.shareBackground.length)];
  }

  static getAllStartBackgrounds() {
    return this.startBackground;
  }
}
