import Vue from 'vue';

Vue.component('icon-vk', require('./vk').default);
Vue.component('icon-fb', require('./fb').default);
Vue.component('icon-tw', require('./tw').default);
Vue.component('icon-tg', require('./tg').default);
